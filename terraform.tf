variable "hcloud_token" {
  sensitive = true # Requires terraform >= 0.14
}

terraform {
  backend "s3" {
    bucket = "src-tf-state"
    key    = "terraform/terraform.tfstate"
    region = "eu-north-1"
  }

  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.33.2"
    }
  }
}

provider "cloudflare" { /* Set CLOUDFLARE_API_TOKEN in environment */ }

provider "hcloud" {
  token = var.hcloud_token
}