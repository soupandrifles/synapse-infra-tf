variable "SSH_PUB_KEY_PATH" {
  type    = string
  default = "~/.ssh/soupandrifle_ed25519.pub"
}

variable "SSH_PRIV_KEY_PATH" {
  type    = string
  default = "~/.ssh/soupandrifle_ed25519"
}

