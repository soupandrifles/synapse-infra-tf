resource "cloudflare_record" "soupandriflesco-org" {
  zone_id = data.cloudflare_zone.soupandriflesco-org.id
  name    = "soupandriflesco.org"
  value   = hcloud_server.synapse.ipv4_address
  type    = "A"
}

resource "cloudflare_record" "mx-soupandriflesco-org" {
  zone_id  = data.cloudflare_zone.soupandriflesco-org.id
  name     = "mail.soupandriflesco.org"
  value    = cloudflare_record.soupandriflesco-org.hostname
  type     = "MX"
  priority = 0
}

resource "cloudflare_record" "sub-soupandriflesco-org" {
  zone_id = data.cloudflare_zone.soupandriflesco-org.id
  name    = "*.soupandriflesco.org"
  value   = cloudflare_record.soupandriflesco-org.hostname
  type    = "CNAME"
}

# For SMTP2GO
resource "cloudflare_record" "return" {
  zone_id = data.cloudflare_zone.soupandriflesco-org.id
  name    = "em1012626.soupandriflesco.org"
  value   = "return.smtp2go.net"
  type    = "CNAME"
}

resource "cloudflare_record" "dkim" {
  zone_id = data.cloudflare_zone.soupandriflesco-org.id
  name    = "s1012626._domainkey.soupandriflesco.org"
  value   = "dkim.smtp2go.net"
  type    = "CNAME"
}

resource "cloudflare_record" "link" {
  zone_id = data.cloudflare_zone.soupandriflesco-org.id
  name    = "link.soupandriflesco.org"
  value   = "track.smtp2go.net"
  type    = "CNAME"
}

# Redirect other domains
resource "cloudflare_record" "redirect_all_domains" {
  for_each = local.redirect_domains
  zone_id  = each.value
  name     = each.key
  value    = cloudflare_record.soupandriflesco-org.hostname
  type     = "CNAME"
}

resource "cloudflare_record" "redirect_all-subdomains" {
  for_each = local.redirect_domains
  zone_id  = each.value
  name     = "*.${each.key}"
  value    = cloudflare_record.soupandriflesco-org.hostname
  type     = "CNAME"
}