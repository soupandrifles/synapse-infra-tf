resource "hcloud_firewall" "synapse" {
  name = "synapse-firewall"

  dynamic "rule" {
    for_each = toset(["80", "443", "8448", "22", "25", "587"])
    content {
      direction = "in"
      protocol  = "tcp"
      port      = rule.key
      source_ips = [
        "0.0.0.0/0",
        "::/0"
      ]
    }
  }
}

resource "hcloud_ssh_key" "synapse" {
  name       = "soupandrifles"
  public_key = file(var.SSH_PUB_KEY_PATH)
}

resource "hcloud_server" "synapse" {
  name         = "synapse"
  server_type  = "cpx31" # 4 cpu, 8gb ram, 160gb disk
  image        = "ubuntu-20.04"
  location     = "hel1"
  backups      = true
  user_data    = file("scripts/provision.sh")
  firewall_ids = [hcloud_firewall.synapse.id]
  ssh_keys     = [hcloud_ssh_key.synapse.name]

  delete_protection  = true
  rebuild_protection = true

  lifecycle {
    prevent_destroy = true
    ignore_changes  = [user_data, ]
  }

  connection {
    type        = "ssh"
    user        = "root"
    private_key = file(var.SSH_PRIV_KEY_PATH)
    host        = self.ipv4_address
  }

  provisioner "file" {
    source      = ".secrets"
    destination = "/root/env"
  }

  depends_on = [
    hcloud_firewall.synapse,
    hcloud_ssh_key.synapse
  ]
}