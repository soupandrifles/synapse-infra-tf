#!/usr/bin/env bash

# install deps
sudo apt-get update -y
sudo apt-get install -y \
    git \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    gnupg \
    lsb-release


# Install Docker
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose

# Fix the host firewall from interfering with docker networking
sysctl -w net.bridge.bridge-nf-call-arptables=0
sysctl -w net.bridge.bridge-nf-call-iptables=0
sysctl -w net.bridge.bridge-nf-call-ip6tables=0

# create prometheus user
sudo groupadd --system prometheus
sudo useradd -s /sbin/nologin --system -g prometheus prometheus

#install node_exporter
curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest| grep browser_download_url|grep linux-amd64|cut -d '"' -f 4|wget -qi -
tar -xvf node_exporter*.tar.gz
cd  node_exporter*/
sudo cp node_exporter /usr/local/bin

sudo tee /etc/systemd/system/node_exporter.service <<EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=default.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter

# checkout monitoring stack
git clone https://gitlab.com/soupandrifles/synapse-monitoring.git /opt/synapse-monitoring

# run setup
CWD=$(pwd)
cd /opt/synapse-monitoring/
./setup.sh
cd $CWD

chown nobody:nogroup /opt/synapse-monitoring

# create synapse user
sudo useradd -r -m -G docker synapse

# checkout the stack
git clone https://gitlab.com/soupandrifles/synapse-stack.git /home/synapse/synapse-stack

# set ownership of dirs to synapse users
chown -R synapse:synapse /home/synapse/synapse-stack

# to prevent a race condition between the terraform file provisioner, and user_data
until [ -f /root/env ] 
do
    echo "No env file yet, waiting 5 seconds and checking again."
    sleep 5
done

# copy env file
cp /root/env /home/synapse/synapse-stack/.env

# set up the stack
cd /home/synapse/synapse-stack
./setup.sh
