data "cloudflare_zone" "soupandrifles-com" {
  name = "soupandrifles.com"
}

data "cloudflare_zone" "soupandrifles-org" {
  name = "soupandrifles.org"
}

data "cloudflare_zone" "soupandrifles-net" {
  name = "soupandrifles.net"
}

data "cloudflare_zone" "soupandriflesco-com" {
  name = "soupandriflesco.com"
}

data "cloudflare_zone" "soupandriflesco-org" {
  name = "soupandriflesco.org"
}

data "cloudflare_zone" "soupandriflesco-net" {
  name = "soupandriflesco.net"
}

# I haven't registered these, in order to keep costs down, and because they're a handful to remember and type.

# data "cloudflare_zone" "soupandriflescollective-com" {
#   name = "soupandriflescollective.com"
# }

# data "cloudflare_zone" "soupandriflescollective-org" {
#   name = "soupandriflescollective.org"
# }

# data "cloudflare_zone" "soupandriflescollective-net" {
#   name = "soupandriflescollective.net"
# }