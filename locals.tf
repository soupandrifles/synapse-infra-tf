locals {
  secrets = file("./.secrets")

  redirect_domains = {
    "soupandriflesco.com" : data.cloudflare_zone.soupandriflesco-com.id,
    "soupandriflesco.net" : data.cloudflare_zone.soupandriflesco-net.id,
    "soupandrifles.com" : data.cloudflare_zone.soupandrifles-com.id,
    "soupandrifles.org" : data.cloudflare_zone.soupandrifles-org.id,
    "soupandrifles.net" : data.cloudflare_zone.soupandrifles-net.id,
  }
}