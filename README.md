# Synapse Infrastructure Terraform

Terraform code for setting up a reasonably secure server on Hetzner Cloud.

Note: Cloud-init will fetch and attempt to build and run [Synapse Stack](https://gitlab.com/soupandrifles/synapse-stack) on initialization. 

## Installation

- Checkout this repo
- Configure the backend to you own bucket in `terraform.tf`
- Create a local `.env` file

```bash
export CLOUDFLARE_API_TOKEN=<cloudflare token>
export AWS_ACCESS_KEY_ID=<aws access key id>
export AWS_SECRET_ACCESS_KEY-<aws secret access key>
export HCLOUD_API_KEY=<Hetzner Cloud API key>
export TF_VAR_hcloud_token=$HCLOUD_API_KEY
```

#### For Synapse Stack only

Create a file in the repo dir called `.secrets` and place any environment variables you want passed into docker-compose at start up. It will be written to the server by Cloud-init as a `.env` file. 

```bash
export PDDATA="./pgdata"
export SYNAPSE_SERVER_NAME="example.com"
export AWS_ACCESS_KEY_ID=<secret>
export AWS_SECRET_ACCESS_KEY=<secret>
export POSTGRES_PASSWORD=<secret>
export AWS_REGION="us-east-1"
export LE_DOMAINS="example.com, www.example.com, matrix.example.com"
export LE_EMAIL="admin@example.com"
```

## Deploying

```bash
$ source .env
$ terraform init
$ terraform plan -out tfplan
$ terraform apply "tfplan"
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.